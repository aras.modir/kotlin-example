package com.aras.modir.kotlinexample.recycleView

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import android.widget.TextView

class ListAdapter (var item:List<String>) : RecyclerView.Adapter<ListAdapter.ViewHolder>(){

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ListAdapter.ViewHolder {
        return ViewHolder(TextView(p0.context))
    }

    override fun getItemCount(): Int {
        return item.size
    }

    override fun onBindViewHolder(p0: ListAdapter.ViewHolder, p1: Int) {
        p0.textview.text = item[p1]
    }

    class ViewHolder(val textview: TextView) : RecyclerView.ViewHolder(textview)

}