package com.aras.modir.kotlinexample

import android.content.Context
import android.util.Log
import android.widget.Toast
import java.net.URL


fun add(x: Int, y: Int): Int {
    return x + y
}


fun showToast(context: Context, msg: String, length: Int = Toast.LENGTH_SHORT) {
    Toast.makeText(context, msg, length).show()
}

// This is Custom Toast which isn't need Context
fun Context.toast(message: CharSequence, duration: Int = Toast.LENGTH_SHORT) {
    Toast.makeText(this, message, duration).show()
}

class Request (val url: String) {
    fun run() {
        val forcastJsonString = URL(url).readText()
        Log.d("application", forcastJsonString)
    }

}
