package com.aras.modir.kotlinexample.recycleView

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import com.aras.modir.kotlinexample.R
import kotlinx.android.synthetic.main.activity_recycle_view.*

class RecycleViewActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_recycle_view)
        val list = findViewById<RecyclerView>(R.id.list_item)
        list.layoutManager = LinearLayoutManager(this)

        val item = listOf(
            "Mon 6/23 - sunny - 31/17",
            "Tue 6/24 - Foggy - 21/8",
            "Wed 6/25 - Cloudy - 22/17",
            "Thurs 6/26 - Rainy - 18/11"
        )
        list.adapter = ListAdapter(item)

    }



}
