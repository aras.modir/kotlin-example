package com.aras.modir.kotlinexample

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.TextView

class MainActivity : AppCompatActivity() {
    private var z: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var text = findViewById<TextView>(R.id.text)

        val name = "123"
        val int: Int
        int = name.toInt()
        Log.d("ali", int.toString())


        showToast(this, "This is $int")

        for (i in 1..100){
            text.text = i.toString()
            Log.d("ali", i.toString())
        }

        z = add(4,6)
        text.text = z.toString()

    }

}
