package com.aras.modir.kotlinexample.RetrofitExample

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import android.widget.TextView
import com.aras.modir.kotlinexample.MainActivity
import com.aras.modir.kotlinexample.R
import com.aras.modir.kotlinexample.RetrofitExample.IMDBPojo.IMDBPojoKotlin
import com.aras.modir.kotlinexample.RetrofitExample.IMDBPojo.IPPojo
import com.aras.modir.kotlinexample.showToast
import com.aras.modir.kotlinexample.toast
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_retrofit.*
import kotlinx.android.synthetic.main.activity_retrofit.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class RetrofitActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_retrofit)

        val nameone = findViewById<TextView>(R.id.text_view_name)
        val director = findViewById<TextView>(R.id.text_view_director)
        val search = findViewById<EditText>(R.id.edit_text_search)
        val imageview = findViewById<ImageView>(R.id.image_view_poster)

//        val api = WebInterface.create()
//        api.getIP().enqueue(object : Callback<IPPojo> {
//            override fun onFailure(call: Call<IPPojo>, t: Throwable) {
//                toast(t.toString())
//            }
//
//            override fun onResponse(call: Call<IPPojo>, response: Response<IPPojo>) {
//                toast(response.body()?.city.toString())
//            }
//
//        })


        button_search.setOnClickListener {
            val search: String = search.text.toString()
            val apiService = WebInterface.create()
            apiService.getMovies(search, "70ad462a").enqueue(object : Callback<IMDBPojoKotlin> {

                override fun onFailure(call: Call<IMDBPojoKotlin>, t: Throwable) {
                    toast("Error: $t ")
                }

                override fun onResponse(call: Call<IMDBPojoKotlin>, response: Response<IMDBPojoKotlin>) =

                    if (response.body()?.actors != " ") {

                        val pojo: IMDBPojoKotlin? = response.body()
                        val actors = pojo?.actors
                        nameone.text = "Title: $actors"
                        director.text = "Director: ${pojo?.director}"
                        Picasso.get().load(pojo?.poster).into(imageview)

                    } else {
                        toast("Your name is not on the list")
                    }
            })
        }


    }
}



