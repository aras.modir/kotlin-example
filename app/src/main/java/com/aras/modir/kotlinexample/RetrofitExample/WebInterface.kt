package com.aras.modir.kotlinexample.RetrofitExample

import com.aras.modir.kotlinexample.RetrofitExample.IMDBPojo.IMDBPojoKotlin
import com.aras.modir.kotlinexample.RetrofitExample.IMDBPojo.IPPojo
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface WebInterface {
    @GET("/")
    fun getMovies(@Query("t") movie: String, @Query("apikey") apikey: String):
            Call<IMDBPojoKotlin>

    @GET("json")
    fun getIP (): Call<IPPojo>

    companion object Factory {
        val baseUrl = "http://www.omdbapi.com/"
        fun create(): WebInterface {
            val retrofit = Retrofit.Builder()
                .baseUrl(baseUrl)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
            return retrofit.create(WebInterface::class.java)
        }
    }
}
