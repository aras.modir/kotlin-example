package com.aras.modir.kotlinexample.RetrofitExample.IMDBPojo

import com.google.gson.annotations.SerializedName

class IPPojo {

    @SerializedName("city")
    val city: String? = ""
}